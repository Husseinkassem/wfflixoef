<?php

class Request
{
    public static function uri() {
        return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    }

    /**
     * Add a pubsf method()
     */
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}