<?php

class Routes
{
    /** Mijn toevoeging! */
    // make it public and GET array and POST array
    protected $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file)
    {
        //eigen object
        $router = new self;
        //verkrijg routes.php
        require $file;
        return $router;
    }
    /**
     * Duidelijk?
     */

    public function define($routes)
    {
        $this->routes = $routes;
    }

    /**
     * @throws Exception
     */

    //verkrijg routes.php
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    public function direct($uri, $requestType)
    {
        return $this->controllerAction(
            ...explode('@', $this->routes[$requestType][$uri])
        );
    }

    public function controllerAction($controller, $action)
    {
        $controller=new $controller;
        if (!method_exists($controller, $action)) {
            throw new Exception("{$controller} doesn't have the {$action} action!");
        }
        return $controller->$action();
    }
}