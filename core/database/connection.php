<?php

class connection
{
public static function make($config)
   {
     try {
         return new PDO(
             $config['dsn']. ';dbname='.$config['name'],
             $config['username'],
             $config['password'],
             $config['options']
         );
     }catch(Exception $e){
         die(var_dump($e ->getMessage()));
     }
    }
}
