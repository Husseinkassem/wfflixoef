<?php
App::bind('config', require 'config.php');
App::bind('database',connection::make(App::get('config')['database']));

function view($name, $data=[]){
    extract($data);
    return require "views/{$name}.view.php";
}