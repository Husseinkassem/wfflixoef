<?php

class TemplateController
{
    /**
     * database connection
     */
    public function __construct()
    {
        $this->conn=App::get('database');
    }
    /**
     *laat alle waardes zien
     */
    public function index()
    {

    }
    /**
     *Create
     */
    public function create()
    {

    }
    /**
     *Store- Save to database
     */
    public function store()
    {

    }
    /**
     *show- shoe one recorde voor uodate
     */
    public function show()
    {

    }
    /**
     *Edit -change a recorde
     */
    public function edit()
    {

    }
    /**
     *Update -save  edited
     */
    public function update()
    {

    }
    /**
     *Destroy- Delete  a recorde
     */
    public function destroy()
    {

    }
}

