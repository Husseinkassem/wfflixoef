<?php

class TeachersController
{
    /**
     * database connection
     */
    public function __construct()
    {
        $this->dbh= new Teacher(App::get('database'));
    }
    /**
     *laat alle waardes zien
     */
    public function index()
    {
        $teachers=$this->dbh->show();
        return view('teachers/teachers',compact('teachers'));
    }
    /**
     *Create
     */
    public function create()
    {
        $this->dbh->create($_POST);
        $teachers=$this->dbh->show();
        return view('teachers/teachers',compact('teachers'));

    }
    /**
     *Store- Save to database
     */
    public function store()
    {
        $this->dbh->store($_POST);
        $teachers=$this->dbh->show();
       return view('teachers/teachers',compact('teachers'));
    }

    /**
     *show- shoe one recorde voor uodate
     */
    public function show()
    {
        $this->dbh->show($_GET);

    }
    /**
     *Edit -change a recorde
     */
    public function edit()
    {
        $this->dbh->edit($_POST);
        $teacher=$this->dbh->show();
        return view('teachers/edit-teacher',compact('teacher'));
    }
    /**
     *Update save  edited
     */
    public function update()
    {
        $teacher=$this->dbh->store($_POST);
        return view('teachers/edit-teacher',compact('teacher'));
    }
    /**
     *Destroy- Delete  a recorde
     */
    public function destroy()
    {
        $this->dbh->destroy($_POST);
        $teacher=$this->dbh->show();
        return view('teachers/teachers',compact('teacher'));

    }
}


