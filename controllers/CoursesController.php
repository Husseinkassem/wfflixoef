<?php

class CoursesController
{
    /**
     * database connection
     */
    public function __construct()
    {
        $this->dbh= new Course(App::get('database'));
    }
    /**
     *laat alle waardes zien
     */
    public function index()
    {
        $courses=$this->dbh->show();
        return view('courses/courses',compact('courses'));
    }
    /**
     *Create
     */
    public function create()
    {
        $this->dbh->create($_POST);
        $courses=$this->dbh->show();
        return view('courses/courses',compact($courses));

    }
    /**
     *Store- Save to database
     */
    public function store()
    {

    }
    /**
     *show- shoe one recorde voor uodate
     */
    public function show()
    {

    }
    /**
     *Edit -change a recorde
     */
    public function edit()
    {
        $course=$this->dbh->edit($_POST);
        return view('course/edit-course',compact('course'));
    }
    /**
     *Update save  edited
     */
    public function update()
    {
        $course=$this->dbh->store($_POST);

    }
    /**
     *Destroy- Delete  a recorde
     */
    public function destroy()
    {
        $courser=$this->dbh->destroy($_POST);
    }
}

