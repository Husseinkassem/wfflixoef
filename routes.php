<?php
$router->get('','PagesController@home');
$router->get('contact','PagesController@contact');

$router->get('teachers','TeachersController@index');

$router->post('teacher','TeachersController@create');

$router->post('delete-teacher','TeachersController@destroy');

$router->post('upd-teacher','TeachersController@edit');

$router->post('teacher-upd','TeachersController@update');

$router->get('courses','CoursesController@index');
$router->get('course','CoursesController@create');
$router->get('del-course','CoursesController@destroy');

$router->get('upd-course','CoursesController@edit');
$router->get('course-upd','CoursesController@update');