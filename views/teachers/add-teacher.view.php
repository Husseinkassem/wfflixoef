<?php require 'views/partials/head.php'?>
<div class="wrapper">
    <h2>Voeg docent toe</h2>
    <p>Vul informatie over docent in</p>

    <form method="post" action="add-teacher.view.php">
        <div class="form outline mb-4">
             <input required type="text" id="name" name="name" class="form-control" />
            <label class="form-label" for="name">Naam</label>
        </div>
        <div class="form outline mb-4">
            <input type="text" id="school" name="school" class="form-control" />
            <label class="form-label" for="school">School</label>
        </div>
        <div class="form outline mb-4">
            <input required type="email" id="email" name="email" class="form-control" />
            <label class="form-label" for="email">Email Address</label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="add teacher" />
        </div>
    </form>
</div>
<?php require 'views/partials/foot.php'?>