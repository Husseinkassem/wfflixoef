<!--show header-->
<?php require 'views/partials/head.php'?>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col"></div>
                    <div class="col-8"><h1>Teachers</h1>
                        <div class="wrapper">
                            <h2>Voeg docent toe</h2>
                            <p>Vul informatie over docent in</p>
<!--Add form-->
                            <form method="post" action="teacher">

<!--Add name of teacher-->
                                <div class="col-4">
                                    <input required type="text" id="namefield" name="name" class="form-control" placeholder="Uw Naam" />
                                    <label  class="form-label" for="namefield">Naam</label>
                                </div>
<!--Add school of teacher-->
                                <div class="col-4">
                                    <input type="text" id="school" name="school" class="form-control" placeholder="Uw School" />
                                    <label class="form-label" for="school">School</label>
                                </div>
<!--Add e-mail of teacher-->
                                <div class="col-4">
                                    <input required type="email" id="email" name="email" class="form-control" placeholder="Uw E-mail"/>
                                    <label class="form-label" for="email">Email Address</label>

                                </div>
                                <div class="col-4">
                                    <input type="submit" class="btn btn-primary" value="add teacher" />
                                </div>
                            </form>
<!--end of Add form-->
                        </div>
<!--Table's data of teacher-->
                        <table class="table table-responsive-lg table-striped">
                            <thead>
                            <tr bgcolor="#1e90ff">
                                <th scope="col" >Action</th>
                                <th scope="col" ></th>
                                <th scope="col" ></th>
                                <th scope="col">Name</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">School</th>
                                <th scope="col">Last Update</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($teachers as $teacher): ?>
                            <tr>

                                <th scope="row"></th>


                                <td>
                                    <a href="delete-teacher?id=<?php echo $teacher->id ?>" class="btn btn-danger">Verwijderen</a>
                                    <a href="upd-teacher?id=<?php echo $teacher->id ?>" class="btn btn-warning">Bewerken</a>

                                </td>
                                <td> <?= $teacher->name ?></td>
                                <td><?= $teacher->school ?></td>
                                <td><?= $teacher->email ?></td>
                                <td> <?= date('d/m/Y',strtotime($teacher->updatedAt))?></td>

                                <?php  endforeach;  ?>


                            </tbody>
                        </table>

                    </div>
                    <div class="col"></div>
                </div>

            </div>

        </div>
    </div>
<!-- show footer-->
<?php require 'views/partials/foot.php'?>