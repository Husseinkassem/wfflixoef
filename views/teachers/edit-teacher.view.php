<?php require 'views/partials/head.php'?>
    <div class="wrapper">
        <h2>Voeg docent toe</h2>
        <p>Vul informatie over docent in</p>

        <form method="post" action="edit-teacher">
            <input type="hidden" name="id" value="<?php echo $teacher['id']?>">
            <div class="form-group">
                <label>Naam</label>
                <input required type="text" name="name" class="form-control" value="<?php echo $teacher['name'] ?>">
            </div>
            <div class="form-group">
                <label>E-mail</label>
                <input required type="text" name="email" class="form-control" value="<?php echo $teacher['email'] ?>">
            </div>
            <div class="form-group">
                <label>Wachtwoord</label>
                <input required type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Edit">
            </div>
        </form>
    </div>
<?php require 'views/partials/foot.php'?>