<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <img src="/public/img/navbar-icon.png" height="60" width="60">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Wfflix</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?=(Request::uri()=== '') ? 'active' :'';?>" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'teachers') ? 'active' : 'teachers';?>" href="teachers">Leraren</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'courses') ? 'active' : 'courses';?> " href="courses">Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'contact') ? 'active' : 'contact';?> " href="contact">Contact</a>
                </li>

                <div class="m-2"  style="right: auto" >
                    <div class="dropdown">
                        <a href="login" class="dropdown-toggle" data-bs-toggle ="dropdown">Log</a>
                        <div class="dropdown-menu">
                            <a href="login" class="dropdown-item">Login</a>
                            <a href="register" class="dropdown-item">Register</a>
                            <a href="reset-password" class="dropdown-item">reset-password</a>
                        </div>
                    </div>
                </div>
            </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
