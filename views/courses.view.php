<?php require 'views/partials/head.php'?>
<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-7"><h1>Courses</h1>
                    <div class="list-group">
                        </div>
                    <ul class="list-group">
                        <?php foreach ($courses as $course): ?>
                        <li class="list-group-item active" aria-current="true"> <?= $course->title ?> </li>
                        <li class="list-group-item"> <?= $course->description ?></li>
                        <li class="list-group-item"> <?= $course->link ?>
                            <a href="<?= $course->link ?>" target="_blank"> <br> Go to the website</a>
                        </li>
                    </ul>
                    <?php endforeach; ?>
            </div>
            <div class="col"></div>
        </div>

    </div>

  </div>
  </div>
<?php require 'views/partials/foot.php'?>>