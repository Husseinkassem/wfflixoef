<?php require 'views/partials/header.view.php'?>
    <div class="wrapper">
        <h2>Voeg docent toe</h2>
        <p>Vul informatie over docent in</p>

        <form method="post" action="edit-course">
            <input type="hidden" name="id" value="<?php echo $course['id']?>">
            <div class="form-group">
                <label>Titel van cursus</label>
                <input type="text" name="title" class="form-control" value="<?php echo $course['title'] ?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Edit">
            </div>
        </form>
    </div>
<?php require 'views/partials/footer.view.php'?><?php
