<?php require 'views/partials/nav.php'?>
<div class="container">
    <div class="form">
        <div class="contact-info">
            <link rel="stylesheet" href="css/stylee.css">
            <h3 class="title">Laten we contact opnemen </h3>
            <p class="text"> Neem contact met ons op met de volgende gegevens. en vul het formulier in met de details. </p>
            <div class="info">
                <div class="social-information"> <i class="fa fa-map-marker"></i>
                    <p>Windesheim,Almere,Nederland</p>
                </div>
                <div class="social-information"> <i class="fa fa-envelope-o"></i>
                    <p>Team-A3@Windesheim.nl</p>
                </div>
                <div class="social-information"> <i class="fa fa-mobile-phone"></i>
                    <p>+31684530257 </p>
                </div>
            </div>
            <div class="social-media">
                <p>Verbind je met ons :</p>
                <div class="social-icons"> <a href="#"> <i class="fa fa-facebook-f"></i> </a> <a href="#"> <i class="fa fa-twitter"></i> </a> <a href="#"> <i class="fa fa-instagram"></i> </a> <a href="#"> <i class="fa fa-linkedin"></i> </a> </div>
            </div>
        </div>
        <div class="contact-info-form"> <span class="circle one"></span> <span class="circle two"></span>
            <form action="#" onclick="return false;" autocomplete="off">
                <h3 class="title">Contact us</h3>
                <div class="social-input-containers"> <input type="text" name="name" class="input" placeholder="Name" /> </div>
                <div class="social-input-containers"> <input type="email" name="email" class="input" placeholder="Email" /> </div>
                <div class="social-input-containers"> <input type="tel" name="phone" class="input" placeholder="Phone" /> </div>
                <div class="social-input-containers textarea"> <textarea name="message" class="input" placeholder="Message"></textarea> </div> <input type="submit" value="Send" class="btn" />
            </form>
        </div>
    </div>
</div>