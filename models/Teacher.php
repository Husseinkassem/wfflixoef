<?php
class Teacher
{

    /**
     * @var $conn = database connection
     */
    protected $conn;

    /**
     * Teacher constructor.
     * @param $conn
     */
    public function __construct()
    {
        $this->conn = App::get('database');
    }



    /**
     * @param $teacher
     * CREATE a new teacher from input from form
     */
    public function create($teacher)
    {

        $stmt = $this->conn->prepare("
                    INSERT INTO teachers(name, email, school) 
                    VALUES (?, ? , ?);
                ");

        $stmt->execute([
            $teacher['name'],
            $teacher['email'],
            $teacher['school']
        ]);
        //redirect to /teachers

    }

    /**
     * @return mixed
     * show / index
     * ORDER BY catch in view to show updated first
     */
    public function show()
    {
        $stmt = $this->conn->query('SELECT * 
                                    FROM teachers
                                    ORDER BY NAME DESC ');
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * @param $teacher = teacher.id
     * Update form
     * @return mixed
     */
    public function edit($teacher)
    {
        $stmt = $this->conn->prepare('SELECT * FROM teachers WHERE id = (?);');
        $stmt->execute([$teacher['id']]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    /**
     * @param $teacher
     * store to database from update form.
     *
     */
    public function store($teacher)
    {
        $stmt = $this->conn->prepare("
                    UPDATE teachers 
                    SET name = (?), 
                        emaila = (?), 
                        school = (?),
                        updatedAt = now()
                    WHERE id = (?);
                ");

        $stmt->execute([
            $teacher['name'],
            $teacher['email'],
            $teacher['school'],
            $teacher['id']
        ]);
        header('Location: teachers');
    }



    /**
     * @param $teacher = teacher.id
     * DELETE
     */
    public function destroy($teacher)
    {
        $stmt = $this->conn->prepare("
                    DELETE FROM teachers
                    WHERE id =  (?); ");

        $stmt->execute([$teacher['id']]);
        header('Location: teachers');
    }
}
