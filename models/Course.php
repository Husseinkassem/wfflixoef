<?php

class Course
{
    protected $conn;
    public function __construct()
    {
        $this->conn=App::get('database');
    }
    //CREATE
    public function create($course){
    $stmt=$this->conn->prepare ("INSERT INTO courses (title, description, link ) 
    values (?,?,?)");
    $stmt->execute([
        $course['title'],
        $course['description'],
        $course['link']
    ]);
    header('location:courses');
    }

    //READ
    public function show(){
        $stmt= $this->conn->query('select * from courses');
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }
    // UPDATE
    public function store($course)
    {
    $stmt=$this->conn->prepare("UPDATE courses SET 
        title='?',
        description='?',
        link='?' where id='?';");
    $stmt->execute ([
        $course['title'],
        $course['description'],
        $course['link'],
        $course['id']
    ]);
    header('location:courses');
    }
    // DELETE
    public function destroy($course){
    $stmt=$this->conn->prepare ("DELETE FROM courses where id=(?)");
    $stmt->execute ([$course['id']]);
    header('location:courses');
    }
    public function edit($course){
        $stmt = $this->conn->prepare('SELECT * FROM courses WHERE id = (?);');
        $stmt->execute([$course['id']]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}